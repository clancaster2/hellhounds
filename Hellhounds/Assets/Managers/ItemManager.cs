﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public GameManager gameManager;

    public List<GameObject> items = new List<GameObject>();
    public List<GameObject> collectedItems = new List<GameObject>();
    public List<GameObject> itemSpawnPoints = new List<GameObject>();
    public List<GameObject> usedSpawns = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Spawn()
    {
        //Generates a spawn point for each item
        for (int i = 0; i < items.Count; i++)
        {
            GameObject spawnPoint = GenerateSpawnPoint();
            //Spawns item at generated spawn point
            GameObject item = Instantiate(items[i], spawnPoint.transform.position, Quaternion.identity);
            item.GetComponent<Script_Item>().manager = this;
        }
        Debug.Log("All items spawned");
    }

    GameObject GenerateSpawnPoint()
    {
        //Creates a reference to a random spawn points
        GameObject thisSpawn = itemSpawnPoints[Random.Range(0, itemSpawnPoints.Count)];
        //Checks if the spawn point has already been used
        if (usedSpawns.Contains(thisSpawn) == false)
        {
            //Occupies the spawn point if not used
            usedSpawns.Add(thisSpawn);
        }
        else
        {
            //If spawn point used, generate new until unused point found
            thisSpawn = GenerateSpawnPoint();
        }
        //Return the generated spawn point.
        return thisSpawn;
    }

    public void itemCollected(GameObject item)
    {
        gameManager.IncreaseDifficulty();
        collectedItems.Add(item);
        checkCount();
    }

    public void checkCount()
    {
        if (collectedItems.Count == items.Count)
        {
            //The player has collected all items, activate win state
            gameManager.WinState();
        }
    }
}
