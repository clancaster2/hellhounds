﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    int difficulty = 1;
    [SerializeField]
    ItemManager itemManager;
    [SerializeField]
    SceneManager sceneManager;
    [SerializeField]
    UIManager uiManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncreaseDifficulty()
    {
        difficulty++;
        Debug.Log(difficulty);
    }

    public void WinState()
    {
        Debug.Log("You are a weiner");
        uiManager.DisplayVictoryUI();
    }
}
