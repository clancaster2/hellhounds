﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManager : MonoBehaviour
{
    public Light mainLight;
    public bool isDay;

    public Color dayColor;
    public Color nightColor;

    public Material skyboxDayMat;
    public Material skyboxNightMat;

    // Start is called before the first frame update
    void Start()
    {
        isDay = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            ToggleSkybox();
        }
    }

    void ToggleSkybox()
    {
        if (isDay)
        {
            mainLight.intensity = 0.025f;
            mainLight.color = nightColor;
            RenderSettings.skybox = skyboxNightMat;
        }
        else
        {
            mainLight.intensity = 1f;
            mainLight.color = dayColor;
            RenderSettings.skybox = skyboxDayMat;
        }
        isDay = !isDay;
    }
}
