﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Enum_Hound_States
{
    RoamFull, RoamGuard, RoamPatrol, Search, Hunt,
}
