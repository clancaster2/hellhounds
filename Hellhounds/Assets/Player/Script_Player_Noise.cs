﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Player_Noise : MonoBehaviour
{
    [SerializeField]
    Rigidbody rb;
    [SerializeField]
    Script_Player_Controller controller;
    [SerializeField]
    GameObject player;

    float noiseRadius;
    public SphereCollider noiseSphere;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity != new Vector3(0, 0, 0))
        {
            noiseRadius = CalculateNoise(controller.isSprinting);
        }
        else
        {
            noiseRadius = 1;
        }
        transform.position = player.transform.position;
        noiseSphere.radius = noiseRadius;
    }

    float CalculateNoise(bool isSprinting)
    {
        float noise = 2;
        if (isSprinting)
        {
            noise *= 3;
        }
        return noise;
    }
}
