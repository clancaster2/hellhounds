﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Player_Camera : MonoBehaviour
{
    //Tracks amount of movement in camera
    Vector2 mouseLook;
    //Helps smooth movement of camera, to attempt to remove jerkiness
    Vector2 smoothV;
    public float sensitivityX = 5.0f;
    public float sensitivityY = 5.0f;
    [SerializeField]
    float smoothing = 2.0f;

    GameObject character;

    // Start is called before the first frame update
    void Start()
    {
        //Retrieves and stores the player character
        character = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        //Retrieves and stores the axes value of the mouse inputs. MD, mouse delta, change in mouse since last update loop
        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

        //Scales the mouse delta by the result of scaling the axes sensitivities by the smoothing factor, 
        //essentially using the desired speed and smoothness to determine the initial change in camera movement this frame
        md = Vector2.Scale(md, new Vector2(sensitivityX * smoothing, sensitivityY * smoothing));
        //Takes the above calculated delta, and uses the lerp function with the smoothing values to interpolate the change in position
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
        //Applies the calculated smoothing value to the current position of the camera
        mouseLook += smoothV;
        //Clamps the rotation range between negative and positive 80, so the player can not continuously rotate their camera up or down in one direciton
        mouseLook.y = Mathf.Clamp(mouseLook.y, -80f, 80f);

        //Applies the calculated change in rotation to the x axis of the camera (up and down)
        //The negative modifier ensures the controls are not inverted
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        //Applies the calculated change in rotation to the y axis of the character, so the whole character turns with the camera (left and right)
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);

    }
}
