﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Player_Torch : MonoBehaviour
{
    public KeyCode toggleKey;
    bool isOn = true;
    Light thisLight;

    public Animator anim;
    //public List<AnimationClip> remainOnAnimations = new List<AnimationClip>();
    //public List<AnimationClip> turnOffAnimations = new List<AnimationClip>();

    public float flickerTimer = 5;
    float timerCurrentMax = 5;
    float timerMin = 1;
    float timerMax = 5;
    int rangeMax = 10;
    int rangeMin = 1;
    int rangeCurrentMax = 10;

    // Start is called before the first frame update
    void Start()
    {
        //Retrieves the light component of this game object
        thisLight = gameObject.GetComponent<Light>();
        anim.SetBool("LightOn", true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(toggleKey))
        {
            Toggle();
        }
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    anim.Play("Flicker_Selector");
        //    Debug.Log("Playing");
        //}
        //While the torch is on, runs the flicker function everytime the timer reaches 0
        if (isOn)
        {
            flickerTimer -= Time.deltaTime;
            if (flickerTimer <= 0)
            {
                Flicker();
            }
        }
    }

    void Toggle()
    {
        //Toggles the light component on and off
        isOn = !isOn;
        anim.SetBool("LightOn", isOn);
        anim.Play("Light_Toggle");
        Debug.Log("Toggle" + " " + isOn);
    }

    void Flicker()
    {
        int chance = Random.Range(0, rangeCurrentMax);
        //Runs a random chance to flicker the light
        if (chance == 0)
        {
            //If the chance succeeds, play the flicker anim, then reset the timer and chance to their maximums
            Debug.Log("Play Flicker");
            anim.Play("Flicker_Selector");
            rangeCurrentMax = rangeMax;
            flickerTimer = timerMax;
        }
        else
        {
            //If flicker chance fails, reduce the timer and increase the chance, so long as they are above and below
            //their respective minimums and maximums, then reset the timer and chance to these new values
            if (timerCurrentMax > timerMin)
            {
               timerCurrentMax -= 1;
            }
            flickerTimer = timerCurrentMax;
            if (rangeCurrentMax > rangeMin)
            {
                rangeCurrentMax -= 1;
            }
        }
    }
}
