﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Player_Controller : MonoBehaviour
{
    [SerializeField]
    float speed = 6.0f, sprintSpeedMod = 1.5f, jumpForce = 100f, maxStamina = 100f, currentStamina = 100f;
    public bool isSprinting;
    float sprintTimer = 0f;
    bool isGrounded;
    public Rigidbody rb;
    float distToGround;
    float gravity = -9.8f;

    public float interactDist = 30f;

    public KeyCode sprintKey;
    public KeyCode jumpKey;
    public KeyCode interactKey;

    public Camera cam;

    // Start is called before the first frame update
    void Start()
    {
        //Turns off the cursor visually and locks it within the game window
        Cursor.lockState = CursorLockMode.Locked;
        rb = this.GetComponent<Rigidbody>();
        distToGround = gameObject.GetComponent<Collider>().bounds.extents.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(sprintKey))
        {
            isSprinting = true;
        }
        else
        {
            isSprinting = false;
        }
        if (Input.GetKeyDown(jumpKey) && IsGrounded())
        {
            Jump();
        }
        //HACK - To solve gravity issue with jumping and velocity and such, players will recieve no control when jumping
        if (IsGrounded())
        {
            if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0)
            {
                Movement(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), isSprinting);
            }
        }

        //Counts down a delay after player stops sprinting, and increases player stamina when not full
        sprintTimer -= Time.deltaTime;
        if (sprintTimer <= 0 && currentStamina < maxStamina)
        {
            currentStamina += 10 * Time.deltaTime;
            if (currentStamina > maxStamina)
            {
                currentStamina = maxStamina;
            }
        }

        if (Input.GetKeyDown(interactKey))
        {
            AttemptInteract();
        }
    }

    void Movement(float translation, float strafe, bool isSprinting)
    {
        //Both translation and strafe are modified by speed variable to achieve desired movement speed
        translation *= speed;
        strafe *= speed;
        if (isSprinting && currentStamina > 0)
        {
            translation *= sprintSpeedMod;
            strafe *= sprintSpeedMod;
            currentStamina -= (20 * Time.deltaTime);
            sprintTimer = 5;
        }

        //translation *= transform.forward.x;
        //Modifies translation and strafe by deltatime, removing frame rate link/dependency
        translation *= Time.deltaTime;
        strafe *= Time.deltaTime;

        //FAILED CODE, BUT GOOD TO LEARN WHAT CANT BE USED 
        //Takes the above variables and modifies the game objects translation in the x and z axes.
        //transform.Translate(strafe, 0, translation);
        //Vector3 movement = new Vector3(strafe, 0, translation);

        //Modifies the velocity with bothh the translation and strafe values in the relevant axes
        rb.velocity = transform.forward * translation;
        rb.velocity += transform.right * strafe;


        if (!IsGrounded())
        {
            rb.velocity += transform.up * gravity;
        }

        //Turns on the cursor visually and unlocks it
        //Will be replaced with a more advanced locking/unlocking system for pause menu
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

    void Jump()
    {
        rb.AddForce(0, jumpForce, 0);
    }

    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.05f);
    }

    void AttemptInteract()
    {
        //Raycasts outwards in the forward direction of the players camera
        RaycastHit hit;
        Physics.Raycast(transform.position, cam.transform.forward, out hit, interactDist);
        Debug.DrawRay(transform.position, cam.transform.forward * interactDist, Color.red);
        //Checks to see if an collider was hit
        if (hit.collider != null)
        {
            //stores the colliders game object
            GameObject hitObj = hit.collider.gameObject;
            //Checks object, and parent object for item script, running the interact function if found
            if (hitObj.GetComponent<Script_Item>() != null)
            {
                Debug.Log("Item found");
                //Debug.Log(hitObj.name);
                hitObj.GetComponent<Script_Item>().Interact();
            }
            if (hitObj.GetComponentInParent<Script_Item>() != null)
            {
                Debug.Log("Item found");
                //Debug.Log(hitObj.transform.parent.name);
                hitObj.GetComponentInParent<Script_Item>().Interact();

            }
        }
    }
}


