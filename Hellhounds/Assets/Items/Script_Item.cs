﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Item : MonoBehaviour
{
    public ItemManager manager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Interact()
    {
        Debug.Log("Item: " + gameObject.name + " activated");
        manager.itemCollected(this.gameObject);
        gameObject.SetActive(false);
    }
}
