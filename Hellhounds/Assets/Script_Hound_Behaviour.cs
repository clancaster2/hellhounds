﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Hound_Behaviour : MonoBehaviour
{
    [SerializeField]
    Script_Hound hound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch (hound.houndState)
        {
            case Enum_Hound_States.RoamFull:
                //Runs the full roam behaviour
                //Moves to the current waypoint, standing sentry and selecting a new point if reached
                if (hound.currentDestination != null)
                {

                    if (hound.CheckWaypointDistance() > hound.distToStop)
                    {
                        hound.MoveToWaypoint();
                    }
                    else if (hound.CheckWaypointDistance() <= hound.distToStop)
                    {
                        if (hound.Sentry() <= 0)
                        {
                            hound.RandomWaypoint();
                        }
                    }
                }
                break;
            case Enum_Hound_States.RoamGuard:
                break;
            case Enum_Hound_States.RoamPatrol:
                break;
            case Enum_Hound_States.Search:
                break;
            case Enum_Hound_States.Hunt:
                break;
        }
    }
}
