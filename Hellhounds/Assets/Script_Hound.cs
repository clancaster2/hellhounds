﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Hound : MonoBehaviour
{
    [SerializeField]
    Rigidbody rb;

    [HideInInspector]
    public Enum_Hound_States houndState;
    public Enum_Hound_States roamType;
    [SerializeField]
    float walkSpeed;
    [SerializeField]
    float runSpeed;
    public Vector3 currentDestination;
    public float distToStop = 1f;
[SerializeField]
    float detectionRadius;
    [SerializeField]
    bool isGuarding;
    [SerializeField]
    GameObject guardedItem;

    float sentryTimer;
    [SerializeField]
    float sentryTimerMax = 5f;

    // Start is called before the first frame update
    void Start()
    {
        houndState = roamType;
        RandomWaypoint();
        sentryTimer = sentryTimerMax;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //This function selects a random position for the object to move to 
    public void RandomWaypoint()
    {
        //Generates a vector 2 within a unit circle (to limit distance)
        Vector2 destinationChange = Random.insideUnitCircle * 10;
        Vector3 modifiedPos = this.gameObject.transform.position;
        //Sets the new destination by adding the generated Vector2 values to the current position of the object
        modifiedPos.x += destinationChange.x;
        modifiedPos.z += destinationChange.y;
        currentDestination = modifiedPos;
        sentryTimer = sentryTimerMax;
    }

    //This function moves the object to the currently given position or "waypoint"
    public void MoveToWaypoint()
    {
        transform.LookAt(currentDestination);
        rb.AddRelativeForce(Vector3.forward * walkSpeed * Time.deltaTime, ForceMode.Force);
    }

    //Runs a simple timer to keep hound stationary at each waypoint for a short time
    public float Sentry()
    {
        sentryTimer -= Time.deltaTime;
        return sentryTimer;
    }

    //This checks the current distance between the object and the current waypoint,
    //will primarily be used in checks to allow some leniency in checks that determine whether 
    //the object has reached its destination 
    public float CheckWaypointDistance()
    {
        float distance = 0;
        //Checks the distance between the current position and the target position
        distance = Vector3.Distance(this.gameObject.transform.position, currentDestination);
        //Returns evaluated distance
        return distance;
    }
}
